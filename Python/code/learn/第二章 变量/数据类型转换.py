"""
字符串转int类型
函数： int(x,[基数]) 将字符串或数字转为正数，若x为浮点数，自动舍弃小数
字符串转int类型时，字符串的数字只能为正数,否则会报异常 print(int("12.5"))  # ValueError: invalid literal for int() with base 10: '12.5'
"""
print(int("12"))  # 12
#print(int("12.5"))  # ValueError: invalid literal for int() with base 10: '12.5'
print("10进制", int("10", 10)) # 10进制 10
print("2进制", int("10", 2)) # 2进制 2
print("16进制", int("1a", 16)) # 16进制 26


"""
boolean类型转换
空字符串 == False； 空格 == True
数值 == True; 0 == False
"""
print("空字符==> ", bool("")) # 空字符==>  False
print("带空格的字符串==> ", bool(" ")) # 带空格的字符串==>  True
print("非0数值-1", bool(-1)) # 数值-1 True
print("数值1", bool(1)) # 数值1 True
print("数值1.1", bool(1.1)) # 数值1.1 True
print("数值0", bool(0)) # 数值0 False
