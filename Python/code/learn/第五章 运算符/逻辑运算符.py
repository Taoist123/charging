num1 = 10
num2 = 20
num3 = 30

# 可以简写为print(num1 > num2 > num3)
print("num1 > num2 and num2 > num3的结果为",num1 > num2 and num2 > num3) # num1 > num2 and num2 > num3的结果为 False
print("num3 > num2 and num2 > num1的结果为", num3 > num2 > num1) # num3 > num2 and num2 > num1的结果为 True
print("num2 > num1 and num2 > num3的结果为", num2 > num1 or num2 > num3) # num2 > num1 and num2 > num3的结果为 True