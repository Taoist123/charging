print(1 in (1,2,3)) # True
print(1 not in (1,2,3)) # False

print('1' in '123abc')  # True
print('1' not  in '123abc')  # False