# 元组中的数据不可变的

## 元组的创建
tuple1 = (1,2,3,"a")
print(tuple1)




### 若元组中只有1个元素，解释权会认为它是括号运算,想避免这种情况，可以在后面追加一个逗号
cerate2 = (2)
cerate3 = (2,)
print(cerate2) # 2
print(cerate3) # (2,)


## 元组与List转换
print(tuple([1,2,3,"a"])) # (1, 2, 3, 'a')

## 元组的一些常用函数和List差不多

### 统计值在元组中出现的次数
print(tuple1.count("a"))
