# List 创建

strList = []
print(strList)
print(type(strList))

# 数据存储

objList = [1,2,"a","b",True,False]
print(objList) # [1, 2, 'a', 'b', True, False]

# 类型转换 ，只能转字符串类型
sList = list("adbhh")
print(sList) # ['a', 'd', 'b', 'h', 'h']

# 索引操作
print(objList[1])

# 切片操作，和字符串一样
print(objList[1:5:2])

# 拼接
print("拼接",objList + sList)

intList = [1,2,3,4,5,6]
# 列表的遍历
# 方法1:
print("列表的遍历方法1==》start")
for i in intList:
    print(i)
print("列表的遍历方法1==》end")

# 方法2:
print("列表的遍历方法2==》start")
for i,j in enumerate(intList):
    print("下标",i,"值",j)
print("列表的遍历方法2==》end")

# 方法3:
print("列表的遍历方法3==》start")

for i in  range(len(intList)):
    print(intList[i])
print("列表的遍历方法3==》end")


# 内置函数
print("内置函数-最大值",max(intList)) # 内置函数-最大值 6
print("内置函数-最小值",min(intList)) # 内置函数-最小值 1
print("内置函数-list长度",len(intList)) # 内置函数-list长度 6

# list方法
# 拼接函数
intList.extend([7,8,9])
# 插入元素
intList.insert(8,2)

# 成员操作
print(1 in intList)




# 删除list操作
del  intList




