"""
是一种无序且无重复元素的数据结构
不允许有重复元素，如果添加重复元素，则会自动过滤，可以进行交集、并集的运算。
与dict类似，是一组key的集合（不存储value)
"""

## 创建

# 空集合
empty1 = set()
empty2 = ()

# 直接创建
newSet1 = {1,2,3,4,5,5}
print(newSet1)

# 通过List创建
print(set([1,2,3]))
# 通过元组创建
print(set((1,2,3)))
# 通过字符串创建
print(set('hello')) # {'e', 'o', 'h', 'l'}
# 通过Dict创建
print(set({'name':'zhangsan','age':18})) # {'name', 'age'}

## 交集、并集

s1 = {1,2,3}
s2 = {2,3,4}
print("交集", s1 & s2)
print("并集", s1 | s2)
