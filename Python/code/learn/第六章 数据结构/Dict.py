"""
dict类似于java中的Map，使用键值对存储数据
键值对之间使用，键和值之间使用：分隔
键必须是唯一的，值可以取任何数据类型，但键只能使用字符串、数字或元组
"""
## Dict 创建、

dCreate = {"name" : "张三","address" :"养老院"}

print(dCreate)

# 新增或修改
# 方式1
dCreate.update({"age":"16"})
print(dCreate)
# 方式2
dCreate["weight"] = 189
print(dCreate)

## 遍历
# 方式1
for k, v in dCreate.items():
    print(k,v)
# 方式2
for i in dCreate:
    print(i,dCreate.get(i))

# 遍历Key 或 value
for key in dCreate.keys():
    print(key)
for value in dCreate.values():
    print(value)
