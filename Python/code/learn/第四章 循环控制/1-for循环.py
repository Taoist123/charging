"""
语法：
    for i in range(数值):
        print()
注意：
    1. i是从0开始的
"""
for i in range(10):
    if i == 4:
        continue
    if i == 5:
        # 占位符，啥都不做，相当于todo
        pass
    if i == 6:
        break
    print(i)
