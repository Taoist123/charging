name = input()
birthday = input()

# input默认用户输入的都是字符串，如果需要转为数字类型，使用int()函数
# 可以使用 type(birthday) 查看type()查看变量类型
print(type(birthday),"类型")
print(2025 - int(birthday))

# 控制台输入和输出结果
# heqiang
# 1995
# <class 'str'> 类型
# 30