

"""
  sep
    string inserted between values, default a space.
  end
    string appended after the last value, default a newline.
  file
    a file-like object (stream); defaults to the current sys.stdout.
  flush
    whether to forcibly flush the stream.
"""
# print(self, *args, sep=' ', end='\n', file=None)
# print("打印的值","分隔符，默认是一个空格","结尾，默认是一个换行符")

# 打印值
print("VALUE1")
# 输出结果：VALUE1

# 使用分割符号代替默认空格分隔符
print("一闪一闪亮晶晶","一闪一闪亮晶晶","一闪一闪亮晶晶",sep="*")
print("一闪一闪亮晶晶","一闪一闪亮晶晶","一闪一闪亮晶晶")
# 输出结果：
# 一闪一闪亮晶晶*一闪一闪亮晶晶*一闪一闪亮晶晶
# 一闪一闪亮晶晶 一闪一闪亮晶晶 一闪一闪亮晶晶


# 使用结束符号，代替默认换行符
print("街溜子1","","鬼火少年2",end="gg")
print("街溜子","","鬼火少年")
# 输出结果：
# 街溜子1  鬼火少年2gg街溜子  鬼火少年; 把默认的换行符替换掉了，所以两行变成了一行


# 格式化输出
year = 2024
month = 2
day = 2
temperature = 15.4
print("今天是 %s年%s月%s日，星期一，今天的天气 晴，气温%f度" %(year, month, day,temperature))

""""
%s string（字符串.py）缩写，代表字符串占位
%d digit（数字）缩写，代表有符号十进制整数占位；%5d 表示显示5为整数；%05d 表示显示5为整数,不足5位，用0补齐
%f float（浮点数）缩写，代表有符号十进制整数占位；%.f 表示对小数格式进行处理； %.1f 表示保留1为小数
%% 转义字符，代表打印%号
%(year, month, day,temperature) 表示用表量对前面的占位进行填充
"""
print("今天是 %s 年 %02d 月 %02d 日，星期一，今天的天气 晴，气温 %.1f 度%%" %(year, month, day, temperature))