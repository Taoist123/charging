# 普通闰年的年份是4的倍数，且不是100的倍数；世纪闰年的年份必须是400的倍数。
yearStr = input("请输入年份")

if yearStr.isdigit():
    year = int(yearStr)
    if (year %4 == 0 and year % 100 != 0 ) or (year % 400 == 0):
        print("是闰年")
    else:
        print("不是闰年")
else :
    print("请输入正确的年份")