# 使用with打开文件时，会自动关闭IO流，即使在发生异常时，也会关闭
with open("test.txt", mode="r", encoding="utf-8") as f:
    content = f.read()
    print(content)
print("over")
