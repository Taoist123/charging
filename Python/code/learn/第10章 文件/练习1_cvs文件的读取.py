import csv

# csv 文件的读取
with open("data.csv", mode="r", encoding="utf-8") as scvFile:
    csv_reader = csv.reader(scvFile)

    # 计算平均成绩
    score = []
    # 读取表头，忽略掉
    next(csv_reader)
    for row in csv_reader:
        score.append(int(row[2]))
    print("平均分：",sum(score)/len(score))
