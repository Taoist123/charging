# 打开文件 w表示会覆盖文件内容，重新写入
f = open("test.txt",mode="w",encoding="utf-8")

# 写入文件
f.write("哈哈哈1\n")
f.write("哈哈哈2\n")
f.write("哈哈哈3\n")

f.writelines(["哈哈哈4\n","哈哈哈5\n"])
# 关闭文件
f.close()

# a代表追加内容的意思
f1 = open("test1.txt",mode="a",encoding="utf-8")
# 写入文件
f1.write("哈哈哈1\n")
f1.write("哈哈哈2\n")
f1.write("哈哈哈3\n")

f1.writelines(["哈哈哈4\n","哈哈哈5\n"])
# 关闭文件
f1.close()