# 方式1： 直接导入整个module
import my_module
result = my_module.add(2,3)
print(result)
print(my_module.author)


my_module.author = 'qianghe'
print("修改模块中的全局变量",my_module.author)

# 方式2 ： 只导入自己需要的方法
from my_module import add
print(add(3,4))

# 方式3： 方法重命名, 用于解决导入的方法有重名冲突
from my_module import add as a
print(a(3,5))

# 方式4: 导入包中的所有方法,便于调用时,不写模块名称
from  my_module import *
print(add(3,4))

