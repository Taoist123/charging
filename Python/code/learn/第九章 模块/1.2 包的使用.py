# 语法 from 包名 import 模块名称[,模块名2,模块名称3...]

# 导入方式1
from my_package import math_module
print(math_module.add(1,2))

# 导入方式2 导入该包下的所有工具类
from my_module import *
print(math_module.add(2,3))