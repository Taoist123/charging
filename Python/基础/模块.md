## 模块

> 相当于Java的工具类。
>
> - 模块 就好比是工具包，要想使用这个工具包中的工具，就需要导入import 这个模块
> - 每一个以扩展名 py 结尾的 python源代码文件都是一个 模块，在模块中定义的全局变量、函数，都是模块能够提供给外界直接使用的工具

### 定义模块

```python
# 定义一个变量
author = "heqiang"

# 定义一个函数
def add(x1, x2):
    return x1 + x2


# 测试自己定义的方法是否能正常运行
# print(add(1,2))
```

> 模块命令，不要带有-等特殊字符，否则 模块导入时，可能无法导入



### 导入模块

```python
# 方式1： 直接导入整个module
import my_module
result = my_module.add(2,3)
print(result)
print(my_module.author)


my_module.author = 'qianghe'
print("修改模块中的全局变量",my_module.author)

# 方式2 ： 只导入自己需要的方法
from my_module import add
print(add(3,4))

# 方式3： 方法重命名, 用于解决导入的方法有重名冲突
from my_module import add as a
print(a(3,5))

# 方式4: 导入包中的所有方法,便于调用时,不写模块名称
from  my_module import *
print(add(3,4))
```



## 包

> 相当于Java 的包。
>
> 包是Python模块的一种组织形式，将多个模块组合在一起，形成一个大的Python工具库。包通常是一个拥有__init__.Py文件的目录，它定义了包的属性和方法。

### 定义包

![image-20250318203348767](模块.assets/image-20250318203348767.png)

```
新建一个包时，会自动生成一个 __init__.py 文件，这个文件不能删
```



### 使用包

```Python
# 语法 from 包名 import 模块名称[,模块名2,模块名称3...]

# 导入方式1
from my_package import math_module
print(math_module.add(1,2))

# 导入方式2 导入该包下的所有工具类
from my_module import *
print(math_module.add(2,3))
```





## 包管理

### pip list

Idea terminal 输入 `pip list` 查看已安装的包， pip是Python的包管理工具。

```

(.venv) PS D:\code\charging\Python\code\learn> pip list
Package Version
------- -------
pip     23.2.1

```

### 镜像设置

> 在没有科学上网的条件下，使用国外镜像网速比较慢，把镜像改成国内镜像

```
# 先试下 https://mirrors.aliyun.com/pypi/simple 网址是否能正常打开，打不开说明镜像地址变了，可以重新找下新的镜像
pip config set global.index-url https://mirrors.aliyun.com/pypi/simple

pip config set install.trusted-host mirrors.aliyun.com
```

