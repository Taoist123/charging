## 官网地址

https://www.python.org/



## Windows安装

### 安装

官网下载安装包，进行安装

注意事项

- 勾选**Use admin privileges when installing py.exe（以管理员权限安装python，可以让其他用户也能使用Python）** 、

- 勾选**Add python.exe to PATH（添加环境变量，可以在任意位置使用cmd）**

  

![388d00566e07b3be98477455aea4443e](Python安装.assets/388d00566e07b3be98477455aea4443e.png)

### 校验

cmd输入`Python`,打印版本信息时，即代表安装成功

```
C:\Users\heqia>Python
Python 3.13.2 (tags/v3.13.2:4f8bb39, Feb  4 2025, 15:23:48) [MSC v.1942 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
```

