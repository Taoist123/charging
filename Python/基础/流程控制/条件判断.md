## 单分支

```python
"""
语法：
  if 布尔条件判断 ：
      下级代码块
"""

weather = "rain"
if weather == "rain" :
    print("小雨")
    print("中雨")
print("回家收衣服")

# 打印结果
# 小雨
# 中雨
# 回家收衣服
```

## 双分支

```python
age = 18

if age >= 18:
    print("去网吧")
else:
    print("回去玩泥巴")
```

## 多分支

```python
age = input("请输入你的年龄")
age = int(age)

if 18 <= age < 50:
    print("去网吧")
elif age >= 50:
    print("钓鱼")
else:
  print("回去玩泥巴")
```

## match

```python
today = "星期一"
match today:
    case "星期一":
        print("上班")
    case "星期二":
        print("上班")
    case "星期三":
        print("上班")
    case "星期四":
        print("上班")
    case "星期五":
        print("摸鱼等下班")
    case _:
        print("德玛西亚")
```

> if 
>
> - if语句不支持模式匹配，只能基于布尔表达式进行条件判断。
>
> match
>
> - match语句是Python3.10及以后版本引入的新特性，主要用于**模式匹配**。
> - match语句允许你根据对象的模式结构来检查对象，并根据匹配的模式执行相应的代码块。
> - match语句特别适用于处理复杂的数据结构，如元组、列表、字典等，以及自定义类的实例
> - 它通过模式匹配提供了更简洁、更直观的方式来处理复杂的条件逻辑。