## 注释

```python

# 我是块注释，#号后面要留一个空格
# 说明1
# 说明2
print(111)

"""
我是多行注释，可以用双引号也可以用单引号
"""
print("2222")


print("行内注释")  # 行内注释：#号与代码之间留2个空格，与代注释之间留有1个空格
```

## print函数

```python

#  print(self, *args, sep=' ', end='\n', file=None)
"""
  sep
    string inserted between values, default a space.
  end
    string appended after the last value, default a newline.
  file
    a file-like object (stream); defaults to the current sys.stdout.
  flush
    whether to forcibly flush the stream.
"""

# print("打印的值","分隔符，默认是一个空格","结尾，默认是一个换行符")

# VALUE1
print("VALUE1")

# 一闪一闪亮晶晶*一闪一闪亮晶晶*一闪一闪亮晶晶
print("一闪一闪亮晶晶","一闪一闪亮晶晶","一闪一闪亮晶晶",sep="*")
# 一闪一闪亮晶晶 一闪一闪亮晶晶 一闪一闪亮晶晶
print("一闪一闪亮晶晶","一闪一闪亮晶晶","一闪一闪亮晶晶")

# 街溜子1  鬼火少年2gg街溜子  鬼火少年; 把默认的换行符替换掉了，所以两行变成了一行
print("街溜子1","","鬼火少年2",end="gg")
print("街溜子","","鬼火少年")

```

