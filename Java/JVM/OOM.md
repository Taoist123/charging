查看应用的内存占用情况

```
 top -p Pid （进程号） 看看应用的内存占用情况
```

```plaintext
top - 12:34:56 up  1:23,  3 users,  load average: 0.00, 0.01, 0.05
Tasks:   1 total,   0 running,   1 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.3 us,  0.1 sy,  0.0 ni, 99.5 id,  0.1 wa,  0.0 hi,  0.0 si,  0.0 st
MiB Mem :  8000.0 total,  1000.0 free,  2000.0 used,  5000.0 buff/cache
MiB Swap:  2000.0 total,  2000.0 free,     0.0 used.  5500.0 avail Mem 

  PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND   
 1234 user1     20   0   50000   2000   1000 S   0.3   0.1   0:00.10 myprocess
```

