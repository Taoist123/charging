## 定位慢方法

1.启动arthas

```
#启动arthas
java -jar .larthas-boot.jar
```

2.选择监听的服务

```
#输入服务对应的数字，然后回车
[INFO] arthas-boot version: 4.0.4
[INFO] Found existing java process, please choose one and input the serial number of the process, eg : 1. Then hit ENTER.
*[1]:8528 com.intellij.idea.Main
[2]: 13732 org.jetbrains.jps.cmdline.Launcher
[3]: 3016
[4]: 13964 com.spdb.speed4j.microservice.Speed4jApplication
[5]: 5292 D:/code/conch-eureka.jal
```

3.输入需要监听的方法

```
#命令说明：trace -E类完全限定名方法名
#命令使用:
#监听一个类中的多个方法 trace -E com...className method1 | method2
#监听多个类中的多个方法 trace -E com...className1|com...className2 method1 | method2
#当命令过长时可以进行\进行换行
```

