

、

## 调优概述

### 解决生产环境遇到的问题

- 发生了内存溢出如何处理
- 生产环境应该给服务器分配多少内存合适
- 如何对垃圾回收器的性能进行调优
- CPU负载飙升如何处理
- 给应用分配多少线程合适
- 不加log，如何确定请求是否执行了某一行代码
- 不加log，如何实时查看某个方法的入参和返回值

> GC调优目的： 减少GC频率和FULL GC出现次数，以较少的内存实现更多的吞吐量。
>
> tips:
>
> - 并发数在合理范围内，吞吐量会上升，并发数超过一定规模时，反而会降低吞吐量





## 为什么要调优

- 防止出现OOM
- 解决OOM
- 减少Full GC出现的频率

## 不同阶段的考虑

- 上线前
- 项目运行阶段
- 线上出现OOM



## 调优概述

### 监控的依据

- 运行日志
- 异常堆栈
- GC日志
- 线程快照
- 对存储快照

### 调优的大方向

- 合理的编写代码
- 充分并合理的使用硬件资源
- 合理地进行JVM调优

## 性能优化的步骤

### 性能监控

todo

> 一种以非强行或者入侵的方式**收集和查看**应用运营性能数据的活动。
>
> 监控通常是指一种在生产、质量评估或者开发环境下实施带有**预防**或**主动性**的活动。
>
> 当相关干系人**提出性能问题却没有提供足够多的线索时**，首先我们需要进行性能监控随后是性能分析。

### 性能分析

todo

> 一种以**入侵方式**收集运行性能数据的活动，它会影响应用的吞吐量和响应性。
>
> 性能分析是针对性能问题的答复结果，关注的问题通常比性能监控更加密集。
>
> 性能分析很少在生产环境下进行，**通常是在质量评估、系统测试、开发环境下进行**，是性能监控之后的步骤。

### 性能调优

todo

> 一种为改善应用响应性或吞吐量而更改参数、源代码、属性配置的活动。

### 性能评价

todo



## 性能监控及诊断工具-命令行

造成Java应用出现性能问题的因素非常多，如线程控制（线程数量）、磁盘读写（从缓存读还是从物理磁盘读）、数据库访问（SQL语句、表结构设计）、网路I/O、垃圾收集等。想要定位这些问题，一款优秀的性能诊断工具必不可少。

> 体会1： 使用数据说明问题，使用知识分析问题，使用工具出来问题。
>
> 体会2：无监控，不调优！

### 命令行工具

命令行工具位于JDK的bin目录，用于获取目标JVM不同方面、不同层次的信息，可以很好的帮助开发人员解决Java应用的一些疑难杂症。

![Snipaste_2023-10-08_22-05-13](JVM优化.assets/Snipaste_2023-10-08_22-05-13.png)

![image-20231008221645880](JVM优化.assets/image-20231008221645880.png)


> - 官方源码：https://hg.openjdk.org/jdk/jdk11/file/1ddf9a99e4ad/src/jdk.jcmd/share/classes/sun/tools
>
> 
>- 把jdk lib包中的tools.jar解压后，进入sun->tools目录，即可看到命令行工具对应的class文件

### jps 查看正在运行的java进程

jps(Java Process Status) : 显示指定系统内所有HotSpot虚拟机进程，对于本地虚拟机进程来说，进程的虚拟机ID和操作系统的进程ID是一致的。



> 基本语法 
>
> - jps [-q] [-mlvV] [<hostid>]
>   - -l 输出应用程序主类的全类名，如果进程执行的是jar包，则输出jar包的完整路径
> - -v 列出虚拟机进程启动时的JVM参数。如 -Xms -Xmx
>   - -q 仅仅显示本地虚拟机ID
> - -m 输出虚拟机进程启动时传递给主类main（）的参数
> - jps [-help]
>
> tips：
>
> - 如果某JAVA进程关闭了默认开启的`UsePerfData`参数（即使用参数-XX: -UsePerfData），那么jps和jstat命令将无法探知该Java进程
>
> - 执行jps命令时，每次jps对应的进程ID都会变化
> - jps [-q] [-mlvV]  > 文件路径， 可以将结果输入到指定的文件中

```
示例 jps -m ,在项目的program arguments 该行输入 study，执行jps -m 控制台会打印出参数
```

![image-20231009214017155](JVM优化.assets/image-20231009214017155.png)

```
示例 jps -l
```

![image-20231009214312132](JVM优化.assets/image-20231009214312132.png)



### jstat

jstat（JVM statistics Monitoring Tool）用于监视虚拟机各自运行状态信息的命令行工具。它可以显示本地或者远程虚拟机进程中的类装载、内存、垃圾收集、JIT编译等运行数据。

**在没有GUI图形界面，只提供了纯文本控制台环境的服务器上，它将是运行期定位虚拟机问题的首选工具。常用于检测垃圾回收问题及内存泄漏问题。**

官方文档：https://docs.oracle.com/javase/8/docs/technotes/tools/unix/jstat.html

#### 基本语法

```
jstat -help|-options
jstat -<option> [-t] [-h<lines>] <vmid> [<interval> [<count>]]
```

语法说明：

- [-t] ：程序已运行多长时间
- [-h<lines>] 没打印多少行就会显示一次表头信息 
- <vmid> JVM进程ID
- <interval> 每隔多少买秒打印一次信息
- [<count>] 共打印几次

```
//控制台执行 jstat -options 可以看到options对应的选项
C:\Users\t>  jstat -options
-class  //显示ClassLoader的相关信息：类的装载、卸载数量、总空间、类装载所消耗的时间等
-compiler //显示JIT编译器编译过的方法、耗时等信息
-gc //显示与GC相关的堆信息。包括Eden区、两个Survivor区、老年代、永久代等的容量、已用空间、GC时间合计等信息。
-gccapacity //显示内容与-gc基本相同，但输出主要关注Java堆各个区域使用到的最大、最小空间。
-gccause //与-gcutil功能一样，但是会额外输出导致最后一次或当前正在发生的GC产生的原因。
-gcmetacapacity 
-gcnew //显示新生代GC状况
-gcnewcapacity //显示内容与-gcnew基本相同，输出主要关注使用到的最大、最小空间
-gcold //显示老年代GC状况
-gcoldcapacity //显示内容与-gcold基本相同，输出主要关注使用到的最大、最小空间
-gcutil //显示内容与-gc基本相同，但输出主要关注已使用空间占总空间的百分比。
-printcompilation //被JIT编译的方法
```

##### 示例： jstat -class

```
C:\Users\t>jstat -class -t -h3 5844 1000 10 //打印5844进程信息，每打印3行显示一次表头，每隔一秒打印一次信息，共打印10次
Timestamp       Loaded  Bytes  Unloaded  Bytes     Time
          136.7    701  1403.4        0     0.0       0.07
          137.7    701  1403.4        0     0.0       0.07
          138.7    701  1403.4        0     0.0       0.07
Timestamp       Loaded  Bytes  Unloaded  Bytes     Time
          139.7    701  1403.4        0     0.0       0.07
          140.7    701  1403.4        0     0.0       0.07
          141.8    701  1403.4        0     0.0       0.07
Timestamp       Loaded  Bytes  Unloaded  Bytes     Time
          142.7    701  1403.4        0     0.0       0.07
          143.7    701  1403.4        0     0.0       0.07
          144.7    701  1403.4        0     0.0       0.07
Timestamp       Loaded  Bytes  Unloaded  Bytes     Time
          145.7    701  1403.4        0     0.0       0.07
```

##### **示例：**jstat -compiler

```
C:\Users\t>jstat -compiler 17152
Compiled Failed Invalid   Time   FailedType FailedMethod
      90      0       0     0.02          0
      
Compiled：编译任务执行数量。
Failed：编译任务执行失败数量。
Invalid：编译任务执行失效数量。
Time：编译任务消耗时间。
FailedType：最后一个编译失败任务的类型。
FailedMethod：最后一个编译失败任务所在的类及方法。    
```

##### 示例：jstat -printcompilation 

```
C:\Users\t>jstat -printcompilation 8396
Compiled  Size  Type Method
      91     66    1 java/util/ArrayList ensureCapacityInternal
```

##### 示例：jstat -gc

![image-20231011212841442](JVM优化.assets/image-20231011212841442.png)

![image-20231011212856193](JVM优化.assets/image-20231011212856193.png)

##### 示例：jstat -gcutil

![image-20231011213922102](JVM优化.assets/image-20231011213922102.png)

> 老年代空间被耗尽,导致OOM，程序中断



示例：jstat -gcause

![image-20231011215004830](JVM优化.assets/image-20231011215004830.png)

> 年轻代中没有足够区域能够存放需要分配的数据而导致GC失败了

####  判断是否出现内存泄漏。

> 方式一：
>
> 1. 在长时间运行的 Java 程序中，我们可以运行jstat命令连续获取多行性能数据，并取这几行数据中 OU 列（即已占用的老年代内存）的最小值。
>
> 2. 然后，我们每隔一段较长的时间重复一次上述操作，来获得多组 OU 最小值。如果这些值呈上涨趋势，则说明该 Java 程序的老年代内存已使用量在不断上涨，这意味着无法回收的对象在不断增加，因此很有可能存在内存泄漏。
>
> 方式二：
>
> 1. 比较Java进程的启动时间以及总GC时间（GCT）列，或者两次测量的间隔时间以及GC时间的增量（增量=第二次GC耗时 - 第一次GC的时间耗时），来得出GC时间占运行时间的比例。
> 2. 如果该比例超过20%，说明目前堆压力较大；如果该比例超过90%，则说明堆中机会没有可用空间，随时可能会OOM



### jinfo 实时查看和修改JVM配置参数

jinfo(Configuration Info for Java)：查看虚拟机配置参数信息，也可用于调整虚拟机的配置参数。

> 在很多情况下，Java应用程序不会指定所有的Java虚拟机参数。而此时，开发人员可能不知道某一个具体的Java虚拟机参数的默认值。在这种情况下，可能需要通过查找文档获取某个参数的默认值。这个查找过程可能是非常艰难的。但有了jinfo工具，开发人员可以很方便地找到Java虚拟机参数的当前值。
>
> jps -v 只能看到主动配置的参数，默认配置的参数不会显示



#### 基本语法 

jinfo [options] pid

| 选项                 | 选项说明                                    |
| -------------------- | ------------------------------------------- |
| -flag <name>         | to print the value of the named VM flag     |
| -flag [+\|-]<name>   | to enable or disable the named VM flag      |
| -flag <name>=<value> | to set the named VM flag to the given value |
| -flags               | to print VM flags                           |
| -sysprops            | to print Java system properties             |
| <no option>          | to print both of the above                  |

####  -flags

![image-20231012220106402](JVM优化.assets/image-20231012220106402.png)

####  -flag [+|-]<name>   

![image-20231012220933136](JVM优化.assets/image-20231012220933136.png)

> jinfo不是对所有的flag参数都支持动态修改。只有被标记为manageable的flag才可以被修改，所以这个修改能力是极其有限的。
>
> linux 执行 java -XX:+PrintFlagsFinal -version | grep manageable 可以看到manageable标记
>
> ![image-20231012221639193](JVM优化.assets/image-20231012221639193.png)
>
> - java -XX:+PrintCommandLineFlags 查看哪些已经被用户或者JVM设置过的详细的XX参数的名称和值

#### -sysprops

```
PS D:\Download\baidu\JVM下篇：性能监控与调优篇\03_代码\JVMDemo2> jinfo -sysprops 17128
Attaching to process ID 17128, please wait...
Debugger attached successfully.
Server compiler detected.
JVM version is 25.271-b09
java.runtime.name = Java(TM) SE Runtime Environment
java.vm.version = 25.271-b09
sun.boot.library.path = D:\Java\jdk1.8.0_271\jre\bin
java.vendor.url = http://java.oracle.com/
......
sun.arch.data.model = 64
sun.java.command = com.atguigu.jstat.ScannerTest
java.home = D:\Java\jdk1.8.0_271\jre
user.language = zh
java.specification.vendor = Oracle Corporation
awt.toolkit = sun.awt.windows.WToolkit
java.vm.info = mixed mode
java.version = 1.8.0_271
java.ext.dirs = D:\Java\jdk1.8.0_271\jre\lib\ext;C:\WINDOWS\Sun\Java\lib\ext
 http://bugreport.sun.com/bugreport/
sun.io.unicode.encoding = UnicodeLittle
sun.cpu.endian = little
sun.desktop = windows

```

扩展：了解

- ```
  java -XX:+PrintCommandLineFlags  查看哪些已经被用户或者JVM设置过的详细的XX参数的名称和值
  ```

  输出结果

  ![image-20231017214321947](JVM优化.assets/image-20231017214321947.png)
  
-  ```
  java -XX:+PrintFlagsInitial 查看所有JVM参数启动的初始值
  java -XX:+PrintFlagsFinal 查看所有JVM参数的最终值
  ```

输出结果： :=代表是被修改过的值

![image-20231017214706480](JVM优化.assets/image-20231017214706480.png)

#### jmap 导出内存映像文件和内存使用情况

**简介：jmap （JVM Memory Map）**

- 获取dump文件（堆转储快照文件，二进制文件），

- 获取目标Java进程的内存相关信息，包括Java堆各区域的使用情况、堆中对象的统计信息、类加载信息等

开发人员可以在控制台输入命令 `jmap help`查看jmap工具的使用方法

官方帮助文档：https://docs.oracle.com/en/java/javase/11/tools/jmap.html

**使用语法**

```cmd
jmap [option] <pid>
    (to connect to running process)
jmap [option] <executable <core>
    (to connect to a core file)
jmap [option] [server_id@]<remote server IP or hostname>
    (to connect to remote debug server)
    
where <option> is one of:
    <none>               to print same info as Solaris pmap
    -heap                to print java heap summary
    -histo[:live]        to print histogram of java object heap; if the "live"
                         suboption is specified, only count live objects
    -clstats             to print class loader statistics
    -finalizerinfo       to print information on objects awaiting finalization
    -dump:<dump-options> to dump java heap in hprof binary format
                         dump-options:
                           live         dump only live objects; if not specified,
                                        all objects in the heap are dumped.
                           format=b     binary format
                           file=<file>  dump heap to <file>
                         Example: jmap -dump:live,format=b,file=heap.bin <pid>
    -F                   force. Use with -dump:<dump-options> <pid> or -histo
                         to force a heap dump or histogram when <pid> does not
                         respond. The "live" suboption is not supported
                         in this mode.
    -h | -help           to print this help message
    -J<flag>             to pass <flag> directly to the runtime system 
  #说明： 这些参数和linux下输入的命令多少会有不同，包括也受jdk版本的影响  
```



**导出dump文件的两种方式：**

1. 手动导出dump文件

   ```
   jmap -dump:format=b,file=<filename.hprof> <pid>
   jmap -dump:live,format=b,file=<filename.hprof> <pid>
   ```

2. 自动导出dump文件

   ```
   -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=<filename.hprof>
   ```

> 说明:
>
> 大多数情况下，导出的都是 -dump:live 文件。导致OOM情况，一般都是无法被GC回收的对象导致的，只导出live对象的文件一般会比较小，便于快速从生产环境导出来，第二文件小，解析的时候加载比较快。

> 说明:
>
> Heap Dump又叫做堆存储文件，指一个Java进程在某个时间点的内存快照。Heap Dump在触
> 发内存快照的时候会保存此刻的信息如下:
>
> + All 0bjects
>
>   Class,fields,primitive values and references
>
> + All Classes
>   ClassLoader,name,super class,static fields
>
> + Garbage Collection Roots
>   Objects defined to be reachable by the JVM
>
> + Thread Stacks and Local Variables
>   The call-stacks of threads at the moment of the snapshot,and per-frame
>   information about local objects

> 说明:
> 1.通常在写Heap Dump文件前会触发一次FulI GC，所以heap dump文件里保存的都是
> Fu11GC后留下的对象信息。
> 2.由于生成dump文件比较耗时，因此大家需要耐心等待，尤其是大内存镜像生成dump文件
> 则需要耗费更长的时间来完成。

> 说明:
>
> 由于jmap将访问堆中的所有对象，为了保证在此过程中不被应用线程干扰，jmap需要借助安全点机制，让所有线程停留在不改变堆中数据的状态。也就是说，由jmap导出的堆快照必定是安全点位置的。这可能导致基于该堆快照的分析结果存在偏差。
> 举个例子，假设在编译生成的机器码申，某些对象的生命周期在两个安全点之间，那么:live选项将无法探知到这些对象。
> 另外，如果某个线程长时间无法跑到安全点，jmap将一直等下去。与前面讲的jstat则不同
> 垃圾回收器会主动将istat所需要的摘要数据保存至固定位置之中，而istat只需直接读取
> 即可。

**内存使用情况**

```
常用的三个命令
-dump
-heap <pid> 输出整个堆空间的详细信息，包括GC的使用、堆配置信息，以及内存的使用情况 
-histo <pid> 输出堆中对象的统计信息，包括类，实例数量和合计容量。-histo:live只统计堆中的存货对象

```

#### jhat: JDK自带堆分析工具

JDK8之后废弃



#### jstack

jstack(JVM stack Trace): 用于生成虚拟机指定进程当前时刻的线程快照(虚拟机堆栈跟踪)。线程快照就是当前虚拟机内指定进程的每一条线程正在执行的方法堆栈的集合。
生成线程快照的作用:可用于定位线程出现长时间停顿的原因，如线程间死锁、死循环、请求外部资源导致的长时间等待等问题。这些都是导致线程长时间停顿的常见原因。当线程出现停顿时，就可以用istack显示各个线程调用的堆栈情况。
官方帮助文档:
https://docs.oracle.com/en/java/javase/11/tools/jstack.html
在thread dump中，要留意下面几种状态
+ **死锁，Deadlock** (重点关注)

+ **等待资源，Waiting on condition** (重点关注)

+ **等待获取监视器，Waiting on monitor entry** (重点关注)

+ **阻塞，Blocked** (重点关注)

+ 执行中，Runnable

+ 暂停，Suspended

+ 对象等待中，Object.wait() 或 TIMED WAITING

+ 停止，Parked

  

**语法**

```
C:\Users\t>jstack -help
Usage:
    jstack [-l] <pid>
        (to connect to running process)
    jstack -F [-m] [-l] <pid>
        (to connect to a hung process)
    jstack [-m] [-l] <executable> <core>
        (to connect to a core file)
    jstack [-m] [-l] [server_id@]<remote server IP or hostname>
        (to connect to a remote debug server)

Options:
    -F  to force a thread dump. Use when jstack <pid> does not respond (process is hung)
    -m  to print both java and native frames (mixed mode) //如果调用本地方法的话，可以显示C/C++的堆栈
    -l  long listing. Prints additional information about locks
    -h or -help to print this help message
```

**示例代码**

```

import java.util.Map;
import java.util.Set;

/**
 * 演示线程的死锁问题
 *
 * @author shkstart
 * @create 下午 3:20
 */
public class ThreadDeadLock {

    public static void main(String[] args) {

        StringBuilder s1 = new StringBuilder();
        StringBuilder s2 = new StringBuilder();

        new Thread(){
            @Override
            public void run() {

                synchronized (s1){

                    s1.append("a");
                    s2.append("1");

                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    synchronized (s2){
                        s1.append("b");
                        s2.append("2");

                        System.out.println(s1);
                        System.out.println(s2);
                    }

                }

            }
        }.start();


        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (s2){

                    s1.append("c");
                    s2.append("3");

                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    synchronized (s1){
                        s1.append("d");
                        s2.append("4");

                        System.out.println(s1);
                        System.out.println(s2);
                    }
                }
            }
        }).start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                Map<Thread, StackTraceElement[]> all = Thread.getAllStackTraces();//追踪当前进程中的所有的线程
                Set<Map.Entry<Thread, StackTraceElement[]>> entries = all.entrySet();
                for(Map.Entry<Thread, StackTraceElement[]> en : entries){
                    Thread t = en.getKey();
                    StackTraceElement[] v = en.getValue();
                    System.out.println("【Thread name is :" + t.getName() + "】");
                    for(StackTraceElement s : v){
                        System.out.println("\t" + s.toString());
                    }
                }
            }
        }).start();
    }


}

```

**示例结果**

![image-20231025220248327](JVM优化.assets/image-20231025220248327.png)





