

官网阅读版本： 6.16

## 特性

- 核心技术： 依赖注入、事件、资源、国际化、校验、数据绑定、类型转换、SPEL、AOP
- 测试：模拟对象、TestContent框架、Spring MVC测试、WebTestClient
- 数据访问: 事务、DAO支持、JDBC、ORM、编组XML。
-  Spring MVC和Spring WebFlux web框架。
- 集成:远程、JMS、JCA、JMX、电子邮件、任务、调度、缓存和可观察性。
- 语言:Kotlin, Groovy，动态语言。

