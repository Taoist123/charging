# 日期和时间函数

|                              |                    |
| ---------------------------- | ------------------ |
| CURDATE                      | 返回当前日期       |
| CURRENT_DATE(), CURRENT_DATE | 等同CURDATE()      |
| DATE_SUB()                   | 从日期中减去时间值 |
| DATE_ADD()                   | 向日期添加时间值   |



## CURDATE()

以`YYYY-MM-DD`或`YYYY-MM-DD`格式返回当前日期，具体格式取决于该函数在字符串或数值上下文中的使用方式。

```sql
mysql> SELECT CURDATE();
        -> '2008-06-13'
mysql> SELECT CURDATE() + 0;
        -> 20080613
```

> **Note**
>
> `SELECT CURDATE() +/-  <数值>` ，并不是日期上的加减，而是十进制的加减。如果使用CURDATE()-1计算前一天，则当前日期为1号的时候就会发生错误。例如当天日期为2023-12-01，使用 `select curdate()-1` 会出现2023-11-00这种情况！

## DATE_ADD(date,INTERVAL expr unit)

## DATE_SUB(date,INTERVAL expr unit)

```sql
mysql> SELECT DATE_ADD('2018-05-01',INTERVAL 1 DAY);
        -> '2018-05-02'
mysql> SELECT DATE_SUB('2018-05-01',INTERVAL 1 YEAR);
        -> '2017-05-01'
mysql> SELECT DATE_ADD('2020-12-31 23:59:59',
    ->                 INTERVAL 1 SECOND);
        -> '2021-01-01 00:00:00'
mysql> SELECT DATE_ADD('2018-12-31 23:59:59',
    ->                 INTERVAL 1 DAY);
        -> '2019-01-01 23:59:59'
mysql> SELECT DATE_ADD('2100-12-31 23:59:59',
    ->                 INTERVAL '1:1' MINUTE_SECOND);
        -> '2101-01-01 00:01:00'
mysql> SELECT DATE_SUB('2025-01-01 00:00:00',
    ->                 INTERVAL '1 1:1:1' DAY_SECOND);
        -> '2024-12-30 22:58:59'
mysql> SELECT DATE_ADD('1900-01-01 00:00:00',
    ->                 INTERVAL '-1 10' DAY_HOUR);
        -> '1899-12-30 14:00:00'
mysql> SELECT DATE_SUB('1998-01-02', INTERVAL 31 DAY);
        -> '1997-12-02'
mysql> SELECT DATE_ADD('1992-12-31 23:59:59.000002',
    ->            INTERVAL '1.999999' SECOND_MICROSECOND);
        -> '1993-01-01 00:00:01.000001'
```

