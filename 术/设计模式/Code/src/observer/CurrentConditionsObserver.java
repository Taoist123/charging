package observer;


/**
 * 当前天气观察者
 */
public class CurrentConditionsObserver implements Observer {
	/**
	 * 持有主题，需要要数据，自己取。或者主题通过update(WeatherSubject weatherSubject)推送数据
	 */
	private WeatherSubject weatherSubject;
	private float temperature;
	private float humidity;
	
	public CurrentConditionsObserver(WeatherSubject weatherSubject) {
		this.weatherSubject = weatherSubject;
		weatherSubject.addObserver(this);
	}

	public void update() {
		this.temperature = weatherSubject.getTemperature();
		this.humidity = weatherSubject.getHumidity();
		display();
	}
	public void display() {
		System.out.println("Current conditions: " + temperature 
			+ "F degrees and " + humidity + "% humidity");
	}
}
