package observer;

/**
 * 主题
 */
public interface Subject {

     /**
      * 注册观察者
      */
     void addObserver(Observer o);
     /**
      * 移除观察者
      */
     void removeObserver(Observer o);

     /**
      * 通知观察者
      */
     void notifyObservers();
}
