package observer;

/**
 * 气象站
 */
public class WeatherStation {

	public static void main(String[] args) {
		WeatherSubject weatherWeatherSubject = new WeatherSubject();
		CurrentConditionsObserver currentConditions = new CurrentConditionsObserver(weatherWeatherSubject);
		StatisticsObserver statisticsDisplay = new StatisticsObserver(weatherWeatherSubject);
		weatherWeatherSubject.setMeasurements(80, 65, 30.4f);
	}
}
