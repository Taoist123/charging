package observer;

/**
 * 观察者
 */
public interface Observer {
	/**
	 * 当气象发生变化时进行更新
	 */
	void update();
}
