package observer;

/**
 * 天气统计观察者
 */
public class StatisticsObserver implements Observer {
	private float maxTemp = 0.0f;
	private float minTemp = 200;
	private float tempSum= 0.0f;
	private int numReadings;

	private WeatherSubject weatherSubject;

	public StatisticsObserver(WeatherSubject weatherSubject) {
		this.weatherSubject = weatherSubject;
		weatherSubject.addObserver(this);
	}

	public void update() {
		float temp = weatherSubject.getTemperature();
		tempSum += temp;
		numReadings++;

		if (temp > maxTemp) {
			maxTemp = temp;
		}

		if (temp < minTemp) {
			minTemp = temp;
		}

		display();
	}

	public void display() {
		System.out.println("Avg/Max/Min temperature = " + (tempSum / numReadings)
			+ "/" + maxTemp + "/" + minTemp);
	}
}
