package command;

/**
 * 接收者（Receiver）-炮兵
 */
public class ArtilleryReceiver {

    public void fire() {
        System.out.println("炮兵开火！");
    }
}
