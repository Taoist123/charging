package command;

/**
 * 具体命令-开枪指令
 */
public class ConcreteAttackCommand implements Command {
    private SoliderReceiver solider;

    public ConcreteAttackCommand(SoliderReceiver soliderReceiver) {
        this.solider = soliderReceiver;
    }

    @Override
    public void execute() {
        solider.shoot();
    }
}