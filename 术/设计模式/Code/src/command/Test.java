package command;

public class Test {
    public static void main(String[] args) {
        GeneralInvoker generalInvoker = new GeneralInvoker();
        //射击
        Command command1 = new ConcreteAttackCommand(new SoliderReceiver());
        generalInvoker.setCommand(command1);
        //开炮
        Command command2 = new ConcreteShellCommand(new ArtilleryReceiver());
        generalInvoker.setCommand(command2);
        generalInvoker.execute();
        //炮兵先撤
        generalInvoker.undo();
        //士兵断后
        generalInvoker.execute();
    }
}