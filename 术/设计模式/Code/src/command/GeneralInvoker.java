package command;

import java.util.ArrayList;
import java.util.List;

/**
 * 将军
 */
public class GeneralInvoker {

    private List<Command> commandList = new ArrayList<Command>();

    /**
     * 发起命令
     */
    public void execute() {
        for (Command command : commandList) {
            command.execute();
        }
    }

    public void setCommand(Command command) {
        commandList.add(command);
    }

    public void undo() {
        commandList.remove(commandList.size() - 1);
    }
}