package command;

/**
 * 具体命令-炮击指令
 */
public class ConcreteShellCommand implements Command {
    private ArtilleryReceiver receiver;

    public ConcreteShellCommand(ArtilleryReceiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public void execute() {
        receiver.fire();
    }
}