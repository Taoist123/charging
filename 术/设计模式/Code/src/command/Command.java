package command;

/**
 * 命令-接口
 */
public interface Command {
   void execute();
}