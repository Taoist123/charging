package decorator;

/**
 * Decorator 装饰者类
 * 用户装饰类, 持有User的引用，用于装饰用户
 */
public class UserDecorator implements User {
    protected User user;

    public UserDecorator(User user) {
        this.user = user;
    }

    public boolean canView() {
        return user.canView();
    }

    public boolean canDownload() {
        return user.canDownload();
    }

    public boolean canDelete() {
        return user.canDelete();
    }
}
