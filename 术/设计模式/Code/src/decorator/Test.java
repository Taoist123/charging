package decorator;

public class Test {

    public static void main(String[] args) {
        User user = new StandardUser();
        System.out.println("当前用户具有的读写删权限" + user.canView() +","+ user.canDownload() +","+ user.canDelete());

        User premierUser = new PremierUserDecorator(user);
        System.out.println("为普通用户分配付费用户角色后读写删权限" + premierUser.canView() +","+ premierUser.canDownload() +","+ premierUser.canDelete());

        User adminUser = new AdminUserDecorator(user);
        System.out.println("为普通用户分配管理员用户角色后读写删权限" + adminUser.canView() +","+ adminUser.canDownload() +","+ adminUser.canDelete());
    }
}
