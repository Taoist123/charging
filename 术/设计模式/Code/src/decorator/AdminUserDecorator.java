package decorator;

/**
 * ConcreteDecorator 具体装饰类
 *
 * admin用户装饰器，给用户赋予admin权限
 */
public class AdminUserDecorator extends UserDecorator {
    public AdminUserDecorator(User user) {
        super(user);
    }

    @Override
    public boolean canView() {
        return true;
    }

    @Override
    public boolean canDownload() {
        return true;
    }

    @Override
    public boolean canDelete() {
        return true;
    }
}
