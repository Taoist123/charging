package decorator;

/**
 * Component 用户接口
 */
public interface User {

    default boolean canView() {
        return false;
    }

    default boolean canDownload() {
        return false;
    }

    default boolean canDelete() {
        return false;
    }
}
