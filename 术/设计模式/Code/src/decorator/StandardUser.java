package decorator;

/**
 * ConcreteComponent 具体用户类
 * 普通用户
 */
public class StandardUser implements User {
    protected boolean view = false;
    protected boolean downLoad = false;
    protected boolean delete = false;

    @Override
    public boolean canView() {
        return view;
    }

    @Override
    public boolean canDownload() {
        return downLoad;
    }

    @Override
    public boolean canDelete() {
        return delete;
    }


    @Override
    public String toString() {
        return "{" +
                "view=" + view +
                ", downLoad=" + downLoad +
                ", delete=" + delete +
                '}';
    }

}
