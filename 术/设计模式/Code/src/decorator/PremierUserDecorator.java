package decorator;

/**
 * ConcreteDecorator 具体装饰类
 * 用户具体装饰类，给用户添加付费权限
 */
public class PremierUserDecorator extends UserDecorator {
    public PremierUserDecorator(User user) {
        super(user);
    }

    @Override
    public boolean canView() {
        return true;
    }

    @Override
    public boolean canDownload() {
        return true;
    }

    @Override
    public boolean canDelete() {
        return user.canDelete();
    }

}
