package chian;

public class Test {

    public static void main(String[] args) {
        //new Handler.Builder()会共用一个实例对象吗？不会，因为Builder是内部类，每次new Handler.Builder()都会创建一个新的实例对象
        Handler.Builder builder = new Handler.Builder();
        //这里就是链式编程，谁在前谁在后看的清清楚楚，明明白白
        builder.addHandler(new ValidateHandler())
                .addHandler(new LoginHandler())
              ;
        Member member = new Member();
        member.setUsername("");
        member.setPassword("");
        builder.minMatch(3).build().doHandler(member);
    }

}
