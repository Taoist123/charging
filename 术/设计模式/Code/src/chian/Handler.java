package chian;

public abstract class Handler<T> {


    protected int mindMatch;
    protected Handler next;

    private void next(Handler next) {
        this.next = next;
    }



    public abstract void doHandler(Member member);

    public static class Builder<T> {
        private Handler<T> head;
        private Handler<T> tail;


        public Builder<T> addHandler(Handler handler) {
            //这里的this是指当前对象，也就是Builder对象
            if (this.head == null) {
                this.head = this.tail = handler;
                return this;
            }
            this.tail.next(handler);
            this.tail = handler;
            return this;
        }

        public Builder minMatch(int minMatch){
            this.head.mindMatch= minMatch;
            return this;
        }
        public Handler<T> build() {
            return this.head;
        }
    }
}
