Maven 本质上是一个插件执行框架；所有工作均由插件完成。

- **Build plugins: ** 构建插件将在构建期间执行，并且应在 POM 的`<build/>`元素中进行配置。
- **Reporting plugins : **报告插件将在站点生成期间执行，并且应在 POM 的`<reporting/>`元素中配置它们。由于报告插件的结果是生成站点的一部分，因此报告插件应该国际化和本地化。您可以阅读有关[我们插件本地化](https://maven.apache.org/plugins/localization.html)以及如何提供帮助的更多信息。

要查看最新列表，请浏览 Maven 存储库，特别是[`org/apache/maven/plugins`](https://repo.maven.apache.org/maven2/org/apache/maven/plugins/)子目录。 *（插件根据类似于标准 Java 包命名约定的目录结构进行组织）*

| 插件                                                         | **Description 描述**                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| 核心插件                                                     | 与默认核心阶段（即清理、编译）相对应的插件。他们也可能有多个目标。 |
| [ `clean`](https://maven.apache.org/plugins/maven-clean-plugin/) | 构建后清理。                                                 |
| [ `compiler`](https://maven.apache.org/plugins/maven-compiler-plugin/) | 编译 Java 源代码。                                           |
| [ `deploy`](https://maven.apache.org/plugins/maven-deploy-plugin/) | 将构建的`artifact`部署到远程存储库。                         |
| [ `install`](https://maven.apache.org/plugins/maven-install-plugin/) | 将构建的`artifact`安装到本地存储库中。                       |
| [ `resources`](https://maven.apache.org/plugins/maven-resources-plugin/) | 将`resources`复制到输出目录以包含在 JAR 中。                 |
| [ `site`](https://maven.apache.org/plugins/maven-site-plugin/) | 为当前项目生成一个站点。                                     |
| [ `verifier`](https://maven.apache.org/plugins/maven-verifier-plugin/) | 对于集成测试有用 - 验证某些条件是否存在。                    |
| **打类型/工具**                                              | **这些插件与打包各自的artifact类型有关。**                   |
| [ `jar`](https://maven.apache.org/plugins/maven-jar-plugin/) | Build a JAR from the current project. 从当前项目构建一个 JAR。 |
| [ `war`](https://maven.apache.org/plugins/maven-war-plugin/) | Build a WAR from the current project. 从当前项目构建 WAR。   |
| [ `app-client/acr`](https://maven.apache.org/plugins/maven-acr-plugin/) | Build a JavaEE application client from the current project. 从当前项目构建 JavaEE 应用程序客户端。 |
| [ `shade`](https://maven.apache.org/plugins/maven-shade-plugin/) | Build an Uber-JAR from the current project, including dependencies. 从当前项目构建 Uber-JAR，包括依赖项。 |
| [ `source`](https://maven.apache.org/plugins/maven-source-plugin/) | Build a source-JAR from the current project. 从当前项目构建源 JAR。 |
| [ `jlink`](https://maven.apache.org/plugins/maven-jlink-plugin/) | Build Java Run Time Image. 构建 Java 运行时映像。            |
| [ `jmod`](https://maven.apache.org/plugins/maven-jmod-plugin/) | Build Java JMod files. 构建 Java JMod 文件。                 |
| **Reporting plugins 报告插件**                               | **Plugins which generate reports, are configured as reports in the POM and run under the site generation lifecycle. 生成报告的插件在 POM 中配置为报告，并在站点生成生命周期下运行。** |
| [ `changelog`](https://maven.apache.org/plugins/maven-changelog-plugin/) | Generate a list of recent changes from your SCM. 从 SCM 生成最近更改的列表。 |
| [ `changes`](https://maven.apache.org/plugins/maven-changes-plugin/) | Generate a report from an issue tracker or a change document. 从问题跟踪器或变更文档生成报告。 |
| [ `checkstyle`](https://maven.apache.org/plugins/maven-checkstyle-plugin/) | Generate a Checkstyle report. 生成 Checkstyle 报告。         |
| [ `doap`](https://maven.apache.org/plugins/maven-doap-plugin/) | Generate a Description of a Project (DOAP) file from a POM. 从 POM 生成项目描述 (DOAP) 文件。 |
| [ `javadoc`](https://maven.apache.org/plugins/maven-javadoc-plugin/) | Generate Javadoc for the project. 为项目生成 Javadoc。       |
| [ `jdeps`](https://maven.apache.org/plugins/maven-jdeps-plugin/) | Run JDK's JDeps tool on the project. 在项目上运行 JDK 的 JDeps 工具。 |
| [ `jxr`](https://maven.apache.org/jxr/maven-jxr-plugin/)     | Generate a source cross reference. 生成源交叉引用。          |
| [ `linkcheck`](https://maven.apache.org/plugins/maven-linkcheck-plugin/) | Generate a Linkcheck report of your project's documentation. 生成项目文档的 Linkcheck 报告。 |
| [ `pmd`](https://maven.apache.org/plugins/maven-pmd-plugin/) | Generate a PMD/CPD report. 生成 PMD/CPD 报告。               |
| [ `plugin-report`](https://maven.apache.org/plugin-tools/maven-plugin-report-plugin/) | Create a plugin documentation for any mojos found in the source tree. 为源树中找到的任何 mojo 创建插件文档。 |
| [ `project-info-reports`](https://maven.apache.org/plugins/maven-project-info-reports-plugin/) | Generate standard project reports. 生成标准项目报告。        |
| [ `surefire-report`](https://maven.apache.org/surefire/maven-surefire-report-plugin/) | Generate a report based on the results of unit tests. 根据单元测试的结果生成报告。 |
| **Tools 工具**                                               | **These are miscellaneous tools available through Maven by default. 默认情况下，这些是通过 Maven 提供的各种工具。** |
| [ `antrun`](https://maven.apache.org/plugins/maven-antrun-plugin/) | Run a set of ant tasks from a phase of the build. 从构建的一个阶段运行一组 Ant 任务。 |
| [ `artifact`](https://maven.apache.org/plugins/maven-artifact-plugin/) | Manage artifacts tasks like buildinfo. 管理工件任务，例如构建信息。 |
| [ `archetype`](https://maven.apache.org/archetype/maven-archetype-plugin/) | Generate a skeleton project structure from an archetype. 从原型生成骨架项目结构。 |
| [ `assembly`](https://maven.apache.org/plugins/maven-assembly-plugin/) | Build an assembly (distribution) of sources and/or binaries. 构建源代码和/或二进制文件的程序集（发行版）。 |
| [ `dependency`](https://maven.apache.org/plugins/maven-dependency-plugin/) | Dependency manipulation (copy, unpack) and analysis. 依赖关系操作（复制、解包）和分析。 |
| [ `enforcer`](https://maven.apache.org/enforcer/maven-enforcer-plugin/) | Environmental constraint checking (Maven Version, JDK etc), User Custom Rule Execution. 环境约束检查（Maven 版本、JDK 等）、用户自定义规则执行。 |
| [ `gpg`](https://maven.apache.org/plugins/maven-gpg-plugin/) | Create signatures for the artifacts and poms. 为工件和 pom 创建签名。 |
| [ `help`](https://maven.apache.org/plugins/maven-help-plugin/) | Get information about the working environment for the project. 获取有关项目工作环境的信息。 |
| [ `invoker`](https://maven.apache.org/plugins/maven-invoker-plugin/) | Run a set of Maven projects and verify the output. 运行一组 Maven 项目并验证输出。 |
| [ `jarsigner`](https://maven.apache.org/plugins/maven-jarsigner-plugin/)[                                          ](javascript:void(0)) |                                                              |

