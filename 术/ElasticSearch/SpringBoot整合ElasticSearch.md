## 整合环境

- elasticsearch 8.x
- JDK 1.8+
- springboot 2.x

## 方案选择

springboot 整合 ES 有两种方案，ES 官方提供的 [Elasticsearch Java API Client](https://www.elastic.co/guide/en/elasticsearch/client/java-api-client/current/index.html) 和 spring 提供的 [Spring Data Elasticsearch]([Spring Data Elasticsearch](https://spring.io/projects/spring-data-elasticsearch))

- Spring：高度封装，用着舒服。缺点是更新不及时，有可能无法使用 ES 的新 API

- ES 官方：更新及时，灵活，缺点是太灵活了，基本是一比一复制 REST APIs，项目中使用需要二次封装。

个人和网上的博客偏向使用ES官方。



## Elasticsearch Java API Client

官方文档：https://www.elastic.co/guide/en/elasticsearch/client/java-api-client/8.11/installation.html#maven

### Maven依赖

```xml
<!-- 如果项目中没有此包，要引入 -->
<dependency>
    <groupId>org.elasticsearch.client</groupId>
    <artifactId>elasticsearch-rest-client</artifactId>
    <version>8.11.4</version>
</dependency>

<dependency>
    <groupId>co.elastic.clients</groupId>
    <artifactId>elasticsearch-java</artifactId>
    <version>8.12.2</version>
</dependency>
 
<!-- 如果有添加springmvc，此包可不引入 -->
<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-databind</artifactId>
    <version>2.12.3</version>
</dependency>
```

如果报错 `ClassNotFoundException: jakarta.json.spi.JsonProvider`，则还需要添加

```xml
<dependency>
    <groupId>jakarta.json</groupId>
    <artifactId>jakarta.json-api</artifactId>
    <version>2.0.1</version>
</dependency>
```

### 连接

```java


// Create the low-level client
RestClient restClient = RestClient
    .builder(HttpHost.create(serverUrl))
    .setDefaultHeaders(new Header[]{
        new BasicHeader("Authorization", "ApiKey " + apiKey)
    })
    .build();

// Create the transport with a Jackson mapper
ElasticsearchTransport transport = new RestClientTransport(
    restClient, new JacksonJsonpMapper());

// And create the API client
ElasticsearchClient esClient = new ElasticsearchClient(transport);

@Configuration  
public class ElasticSearchConfig {  
 
    @Bean  
    public ElasticsearchClient esClient() {  
        // ES服务器URL  
        String serverUrl = "http://127.0.0.1:9200";  
        // ES用户名和密码  
        String userName = "xxx";  
        String password = "xxx";  
 
        BasicCredentialsProvider credsProv = new BasicCredentialsProvider();  
        credsProv.setCredentials(  
                AuthScope.ANY, new UsernamePasswordCredentials(userName, password)  
        );  
 
        // Create the low-level client,
        //Elasticsearch的官方low-level客户端。 它允许通过http与Elasticsearch集群进行通信。 不会对请求进行编码和响应解码。
        RestClient restClient = RestClient  
                .builder(HttpHost.create(serverUrl))  
                .setHttpClientConfigCallback(hc -> hc.setDefaultCredentialsProvider(credsProv))  
                .build();  
 
        // Create the transport with a Jackson mapper
        ElasticsearchTransport transport = new RestClientTransport(restClient, new JacksonJsonpMapper());  
        
        // And create the API client
        return new ElasticsearchClient(transport);  
    }  
 
}
```



## 参考链接

```
https://www.javaedit.com/archives/250
```

