## Spring出现历史原因

在Sring没有出现之前，javaEE解决方案主要是使用EJB（Enterprise JavaBean）。但是EJB运行环境苛刻，代码移植性较差，属于重量级框架.当时 Rod Johnson 在他的著作《Expert One-on-One J2EE Design and Development》中提出了一种轻量级的企业级应用程序开发方法，这种方法基于组件和依赖注入的思想。随后，Rod Johnson 开发了一个名为 iBatis 的 ORM 框架，并将它与他的轻量级开发方法结合起来，形成了 Spring 框架的原型。

### 1. EJB存在问题

~~~markdown
1. 运行环境苛刻
	1.1 EJB只能运行在特定的服务器上，如Weblogic、Websphere这样的特定的应用服务器上，但是这样服务器是商用付费软件，中小型公司需要增加付费成本。
	1.2 付费软件代码闭源，难以根据需求对源码进行修改和定制。
2. 可移植性差
	2.1 EJB运行在Weblogic上，必须实现Weblogic特定的接口。移植到Websphere服务器上面，又必须实现Websphere的接口，代码难以复用。
~~~

### 2. 为什么是Spring

~~~markdown
Spring是一个轻量级的JavaEE解决了解决方案，整合了众多优秀设计模式。
~~~

- 轻量级

~~~markdown
1. Spring对运行环境没有要求，可以运行在免费的Tomcat、jetty上，也可以运行在商用的Weblogic、Websphere上
2. 代码移植性高，运行在不同服务器上，无需实现不同的接口
~~~

- javaEE解决方案
<div align="left">    
	<img src="Spring解析.assets/Snipaste_2024-05-24_21-04-58.bmp">
</div>


~~~markdown
javaEE分层开发过程中，struts只能解决视图层问题、Mybatis只能解决DAO层问题。
但是Spring可以通过SpringMvc解决Controller问题、通过AOP解决Service事务和日志等问题、JDBCTemplate或者整合Hibernate、Mybatis来解决Dao问题，所以Spring比传统的框架解决问题更全面。所以称Spring为一个完整的解决方案体系。
~~~

- 整合设计模式

  ~~~markdown
  整合了工厂模式、代理、策略、模板。。
  ~~~

  