## 关闭共享索引

小项目和内网项目一般用不到共享索引

![image-20241129161428068](IDEA优化.assets/image-20241129161428068.png)

```
关闭方式： Tools -> Shared Indexes -> Public Shared Indexes -->下拉框选择 Don't download, use local indexes
```



## 增加IDEA JVM堆内存

1. 开启内存监控

![image-20241129213616238](IDEA优化.assets/image-20241129213616238.png)

2. 查看内存占用情况

![image-20241129213803805](IDEA优化.assets/image-20241129213803805.png)

3. 若项目卡顿时，内存占比比较高，根据自己电脑适当调大内存为2048,4096...

![image-20241129214037012](IDEA优化.assets/image-20241129214037012.png)



参数说明

```
-Xmx4096m    // 最大内存堆大小4G
-Xms2048m     // 初始内存分配大小为：2G
```

 ![image-20241129215356907](IDEA优化.assets/image-20241129215356907.png)

若修改后，右下角内存未变大，重启IDEA。若是还不起作用，清理缓存，File ->  Invalidate Caches，选择默认的勾选即可。

4. 关闭内存

   若增加内存后，项目卡顿有明显好转后，再右键选择IDEA右侧地图，取消内存监控，毕竟监控也是要耗用性能的。



## 关闭自动更新检测

```
 file ->settings -> 在左侧搜索框输入updates关键字，取消勾选 "Automatically check updates for" 
 如果是内网环境，下面的 "check for plugin updates",也可以取消勾选。
```

![image-20241208205501189](IDEA优化.assets/image-20241208205501189.png)

## 禁用或卸载不用的插件

插件也是影响IDEA卡顿的 一个重要因素：一些不常用的插件，或者从来不用的插件，禁用或者卸载掉,不知道插件的作用，网上搜一下。

`file `->`settings` -> 在左侧搜索框输入`plugin`关键字,查看已安装的插件。

## ![image-20241208205755255](IDEA优化.assets/image-20241208205755255.png)参考链接

```
https://www.jetbrains.com/help/idea/tuning-the-ide.html#configure-platform-properties
```

