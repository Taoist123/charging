## maven 设置多个仓库

有两种不同的方式可以指定多个存储库的使用。第一种方法是在 POM 中指定要使用的存储库。这在构建概要文件内部和外部都支持

```
 <project>
...
  <repositories>
    <repository>
      <id>my-repo1</id>
      <name>your custom repo</name>
      <url>http://jarsm2.dyndns.dk</url>
    </repository>
    <repository>
      <id>my-repo2</id>
      <name>your custom repo</name>
      <url>http://jarsm2.dyndns.dk</url>
    </repository>
  </repositories>
...
</project>
```

另一种指定多个存储库的方法是在`${user.home}/.m2/settings.xml`或者 `${maven.home}/conf/settings.xml`文件中 新建 `profile`信息如下:

```
 <settings>
 ...
 <profiles>
   // 第一个仓库地址
   <profile>
     <id>nexus</id>
     <repositories>
       <repository>
         <id>my-repo2</id>
         <name>your custom repo</name>
         <url>http://jarsm2.dyndns.dk</url>
       </repository>
     </repositories>
   </profile>
    // 第二个仓库地址
    <profile>
      <id>aliyun</id>
      <repositories>
        <repository>
          <id>aliyun</id>
          <url>https://maven.aliyun.com/repository/public</url>
          <releases><enabled>true</enabled></releases>
          <snapshots><enabled>true</enabled></snapshots>
        </repository>
      </repositories>
      <pluginRepositories>
        <pluginRepository>
          <id>aliyun</id>
          <url>https://maven.aliyun.com/repository/public</url>
          <releases><enabled>true</enabled></releases>
          <snapshots><enabled>true</enabled></snapshots>
        </pluginRepository>
      </pluginRepositories>
    </profile>      
 </profiles>

 <activeProfiles>
   <activeProfile>nexus</activeProfile>
   <activeProfile>aliyun</activeProfile>
 </activeProfiles>
 ...
</settings>
```

如果您在`profiles` 中指定 `repository` 存储库，需要激活该特定`profiles`,我们通过在 `activeProfiles` 中进行配置

你也可以通过执行以下命令来激活这个配置文件:

```
 mvn -Pnexus ...
```

正常maven 的settings.xml配置完成profiles之后,可以在idea中进行切换

![27a59f560677416e9eff87ac488996d3~tplv-k3u1fbpfcp-zoom-in-crop-mark_1512_0_0_0](Maven 多仓库和镜像配置.assets/27a59f560677416e9eff87ac488996d3tplv-k3u1fbpfcp-zoom-in-crop-mark_1512_0_0_0.webp)