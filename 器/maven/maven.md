

# 简介

## 什么是Maven

Maven是一个跨平台的项目管理工具。作为Apache组织的一个颇为成功的开源项目，其主要服务于基于Java平台的项目创建，依赖管理和项目信息管理。maven是Apache的顶级项目，解释为“专家，内行”，它是一个项目管理的工具，maven自身是纯java开发的，可以使用maven对java项目进行构建、依赖管理。


## Maven的作用

- 依赖管理
    - 依赖指的就是是 我们项目中需要使用的第三方Jar包, 一个大一点的工程往往需要几十上百个Jar包,按照我们之前的方式,每使用一种Jar,就需要导入到工程中,还要解决各种Jar冲突的问题.
        Maven可以对Jar包进行统一的管理,包括快速引入Jar包,以及对使用的 Jar包进行统一的版本控制
- 一键构建项目
     - 之前我们创建项目,需要确定项目的目录结构,比如src 存放Java源码, resources存放配置文件,还要配置环境比如JDK的版本等等,如果有多个项目 那么就需要每次自己搞一套配置,十分麻烦
    Maven为我们提供了一个标准化的Java项目结构,我们可以通过Maven快速创建一个标准的Java项目.

# 安装

## 前置准备工作：

Maven 是一个 Java 工具，因此必须安装Java才能安装maven，且Java必须在环境变量中

```
java -version
```

必须显示正确的版本号



防火墙

## Windows

### 安装包下载

官网地址：http://maven.apache.org/download.cgi


![在这里插入图片描述](maven.assets/e8ec0f6ebddf22a93389013cfd2cc1a3.jpeg)

### maven安装

Maven 下载后，将 Maven 解压到一个没有中文没有空格的路径下，比如:H:\software\maven 下面。 解压后目录结构如下：

![在这里插入图片描述](maven.assets/fa4b923712639df536e7a94643fd566a.jpeg)

- bin:存放了 maven 的命令
- boot:存放了一些 maven 本身的引导程序，如类加载器等
- conf:存放了 maven 的一些配置文件，如 setting.xml 文件
- lib:存放了 maven 本身运行所需的一些 jar 包

### 环境变量配置

因为maven是zip包格式，没有安装程序，所以需要配置环境变量。

配置 MAVEN_HOME ，变量值就是你的 maven 安装的路径（bin 目录之前一级目录）

![在这里插入图片描述](maven.assets/5725ba0f4d6bbda4b2258908b89bfb30.jpeg)

将MAVEN_HOME 添加到Path系统变量

![在这里插入图片描述](maven.assets/ccabf8a831c9dd07dae9f920c8732e9f.jpeg)

### 安装测试

win+R 打开dos窗口，通过 命令检查 maven 是否安装成功

```
mvn --version
```

控制台应该打印你安装的maven版本信息,例如：

```
Apache Maven 3.6.3 (cecedd343002696d0abb50b32b541b8a6ba2883f)
Maven home: D:\apache-maven-3.6.3\apache-maven\bin\..
Java version: 1.8.0_232, vendor: AdoptOpenJDK, runtime: C:\Program Files\AdoptOpenJDK\jdk-8.0.232.09-hotspot\jre
Default locale: en_US, platform encoding: Cp1250
OS name: "windows 10", version: "10.0", arch: "amd64", family: "windows"
```



## Maven 配置

### 仓库位置修改（可选）

maven仓库默认是在 C盘 .m2 目录下,通常情况下,为防止重装系统C盘被个格式化等....一般不安装在C盘

1. maven安装目录中,进入 conf文件夹, 找到 settings.xml 文件

   ![在这里插入图片描述](maven.assets/99d3266d580eee4a3794483fbb52dd5f.jpeg)

2. 打开 settings.xml文件，进行如下配置如下：

   ![在这里插入图片描述](maven.assets/4c0dca2e83940833718381751ec4bf02.jpeg)

### 镜像地址修改（可选）

Maven默认的远程仓库是在国外, 所以下载jar包时速度会非常慢, 一般推荐国内的阿里云，腾讯等国内镜像仓库，但需注意，有时候国内仓库地址可能会更换

1. 打开 settings.xml,找到 标签 , 下面的内容复制到 中 即可

   ```xml
   <mirror>
       <id>alimaven</id>
       <name>aliyun maven</name>
       <url>
           <!-- 阿里镜像仓库地址-->
           http://maven.aliyun.com/nexus/content/groups/public/
       </url>
       <mirrorOf>central</mirrorOf>        
   </mirror>
   ```

   







### 引用

```
https://blog.csdn.net/u012660464/article/details/114113349
```

