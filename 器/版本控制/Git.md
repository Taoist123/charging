## GIT GUI

### 工作页简介

![20190227124259460](Git.assets/20190227124259460.png)

### 提交变更

1. 选中新增和修改的文件

![image-20230918221349034](Git.assets/image-20230918221349034.png)

2. stage to Commit 、Stage changed Files To Commit

![image-20230918221526854](Git.assets/image-20230918221526854.png)



3. Cimmit Message 填写commit信息后，点击commit
4. 点击push





## GIT 命令行

### 查看git版本

```
$ git --version
```

### 创建版本库

```
git init命令的后面直接输入目录名称，自动完成目录的创建
$ cd /path/to/my/workspace
$ git init demo
初始化空的 Git 版本库于 /path/to/my/workspace/demo/.git/
$ cd demo
```

> **git init**命令在工作区创建了隐藏目录`.git`。
>
> 这个隐藏的`.git`目录就是Git版本库（又叫仓库，repository）
>
> `.git`版本库目录所在的目录，即`/path/to/my/workspace/demo`目录称为**工作区**

### 提交操作

```
在工作区中创建一个文件welcome.txt,将新建立的文件添加到版本库后进行提交
$ git add welcome.txt
$ git commit -m "initialized"

//输出结果
[master（根提交） 7e749cc] initialized
 1 个文件被修改，插入 1 行(+)
 create mode 100644 welcome.txt
```

> git add -A  添加所有变化
> git add -u  添加被修改(modified)和被删除(deleted)文件，不包括新文件(new)
> git add .   添加新文件(new)和被修改(modified)文件，不包括被删除(deleted)文件

> Git和大部分其他版本控制系统都需要再执行一次提交操作，对于Git来说就是执行**git commit**命令完成提交。在提交过程中需要输入提交说明，这个要求对于Git来说是强制性的，不像其他很多版本控制系统（如CVS、Subversion）允许空白的提交说明。在Git提交时，如果在命令行不提供提交说明（没有使用`-m`参数），Git会自动打开一个编辑器，要求您在其中输入提交说明，输入完毕保存退出。
>
> 命令输出的第一行还显示了当前处于名为`master`的分支上，提交ID为7e749cc，且该提交是该分支的第一个提交，即根提交（root-commit）。
>
> 命令输出的第二行开始显示本次提交所做修改的统计：修改了一个文件，包含一行的插入。

### 打印日志美化

```
$ git log --pretty=fuller
```



查看提交日志

```
$ git log
```



查看当前代码所在分支

```
$ git branch
带*号的就是当前所在分支
```



添加文件

```
git add -i
```

